package fxmlexample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * https://docs.oracle.com/javafx/2/get_started/fxml_tutorial.htm
 *
 *  - FXMLExample.java. This file takes care of the standard Java code required for an FXML application.
 *  - Sample.fxml. This is the FXML source file in which you define the user interface.
 *  - FXMLExampleController.java. This is the controller file for handling the mouse and keyboard input.
 */
public class FXMLExample extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("fxml_example.fxml"));

        Scene scene = new Scene(root, 400, 275);

        stage.setTitle("FXML Welcome");
        stage.setScene(scene);
        stage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
